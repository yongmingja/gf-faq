HELP CENTER APPLICATION - WEBSITE

version:
0.5
12 March 2018

Updates:
- add User and Access level
- add ui wyswyg
progress = 80%

version:
1
26 March 2018

Updates:
- add settings (general)
- add links 
- add socmed
- add Contact US + captcha for user send message
- add Contact us backend, notification message, view  and delete

- -some fixes

version:
Final 1.0
2 April 2018

Updates:
- add backup/restore
- add setup db

fixes:
- homepage layout (centered categories)

version:
Final 1.1
3 April 2018
fixes:
- jika Q & A pilih kategori parent maka tidak muncul pilih kategori parent (hilangi yang parent kategori)
- kategori tidak boleh pilih sendiri.
- kategori yang sudah ada anak tidak boleh ganti kategori.
- Cari hanya berdasarkan Q. kalau bisa cari bisa berdasarkan A juga.

version:
Final 1.2
4 April 2018
fixes:
- pisih sub kategori langsung
- lokasi logout ke homepage
- root kategori bisa di rename tapi tidak bisa dihapus, dan tidak bisa dirubah dari root
- lokasi add/insert telah dipindahkan 

version:
Final 1.3
5 April 2018
fixes:
- add input by and edit by
- fix homepage responsive
- fix contact us responsive
- fix insert/update warning
- fix empty sub category links
