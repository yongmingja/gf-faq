<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['apps']      = 'Universitas Universal';
$config['version']   = '0.0.2';
$config['webmaster'] = 'TIF';
$config['email']     = 'tif.uvers@gmail.com';
$config['website']   = 'http://uvers.ac.id';