<?php

class Login extends Controller {

	function __construct() {
		parent::__construct();

		$role = $this->login;
		if($role == 0)
		{
		    header('location: '.URL.'dashboard');
		}
		elseif($role == 1)
		{
		    header('location: '.URL.'dashboard');
		}
	}
	
	function index() 
	{	
		$this->view->msg = "404 Error Page";
		$this->view->render('login/404');
	}
	function GFSOFT() 
	{	
		$this->view->msg = '';
		$this->view->render('login/index',$noInclude = true);
	}
	function run()
	{
		$sth = $this->model->run();
		$row = mysqli_fetch_assoc($sth);
		$count =  mysqli_num_rows($sth);
		if ($count > 0) {
			// login
			Session::init();			 
			if (!empty($row)){
				Session::set('userID', $row['id']);
			}
			header('location: '.URL.'dashboard');
		} else {			
			$this->view->msg = '<p style="color: #ff4040;text-align:center;margin:0;margin-top:-19px;">Username dan Password salah!</p>';
			$this->view->render('login/index',$noInclude = true);
		}
	}
	
}