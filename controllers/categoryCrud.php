<?php 

class CategoryCrud extends Controller {
	
	function __construct(){
		parent::__construct();
		$role = $this->login;
		if ($role == 99) {
			Session::destroy();
			header('location: '.URL.'login');
			exit;
		}
	}

	function index() {
		$this->view->msg = '

		';
		$this->view->cName = '';
		$this->view->parent = '';
		$sth = $this->model->viewCrud($id = false);	
		$this->view->sth = $sth;
		$this->view->title = "Insert Categories";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/categoryCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function view() {
		$this->view->sth = $this->model->viewCrud($id = false);	
		$this->view->sth2 = $this->model->viewCrud2();
		$this->view->sth3 = $this->model->viewCrud3();
		$this->view->title = "Categories";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/viewCategory',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}

	function delete($id = false) {
		$sth = $this->model->deleteCrud($id);	
		header('location: '.URL.'categoryCrud/view');
	}
	function input() {
		$db = new Database();
		$sth = $this->model->inputCrud();
		$this->view->sth = $this->model->viewCrud($id = false);
		$this->view->title = "Input Categories";
		$this->view->cName = $_POST['cName'];
		$this->view->parent = $_POST['parent'];
		$count =  @mysqli_num_rows($sth);
		if ($count > 0)	{
			$this->view->msg = '<button class="btn btn-danger" disabled>kategori <i>'.$_POST['cName'].'</i> sudah ada</button>';
		} else {
			$this->view->msg = '<button class="btn btn-primary" disabled>Berhasil menambahkan kategori <i>'.$_POST['cName'].'</i></button>';
		}
		if(! $sth ) {
	       	die('Could not enter data: ' . mysqli_error($db));
	    }	
		// $this->view->msg = $this->model->msg;
	    $this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/categoryCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);	
				
	}
	function edit($id = false) {
		$sth = $this->model->viewCrud($id);
		$this->view->sth1 = $this->model->viewCrud3();
		$row=mysqli_fetch_assoc($sth);	
		$this->view->msg = '';
		$this->view->title = "Update Categories";
		$this->view->id = $row['id'];
		$this->view->cName = $row['cName'];
		$this->view->parent = $row['parent'];
		$this->view->sth = $this->model->viewCrud($id = false);				
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/editCategory',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function editProc() {
		$db = new Database();
		$sth = $this->model->editCrud();	
		$count =  @mysqli_num_rows($sth);
		if ($count > 0)	{
			$this->view->title = "Update Categories";
			$this->view->cName = $_POST['cName'];
			$this->view->parent = $_POST['parent'];
			$this->view->sth = $this->model->viewCrud($id = false);	
			$this->view->msg = '<button class="btn btn-danger" disabled>kategori <i>'.$_POST['cName'].'</i> sudah ada</button>';
			$this->view->render('admin/head',$noInclude = true);
			$this->view->render('crud/editCategory',$noInclude = true);
			$this->view->render('admin/foot',$noInclude = true);
		} else {
			header('location: '.URL.'categoryCrud/view');
		}
		if(! $sth ) {
	    	die('Could not enter data: ' . mysqli_error($db));
	    }	
		$this->view->msg = $this->model->msg;	    
	}
}