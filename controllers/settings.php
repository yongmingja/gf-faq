<?php 

class Settings extends Controller {
	
	function __construct(){
		parent::__construct();
		$role = $this->login;
		if ($role == 99) {
			Session::destroy();
			header('location: '.URL.'login');
			exit;
		}
	}

	function index() {
		$this->view->title = "Settings";
		$this->view->sth = $this->model->viewGeneral();
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('admin/settings',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function social() {
		$this->view->title = "Social Media";
		$this->view->sth = $this->model->viewSocial();
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('admin/settings',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function link() {
		$this->view->title = "Link";
		$this->view->sth = $this->model->viewLink();
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('admin/settings',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	function edit($id = false) {
		$sth = $this->model->viewPart($id);
		$row=mysqli_fetch_assoc($sth);	
		$this->view->title = "Update ".$row['variable'];
		$this->view->value = $row['value'];
		$this->view->variable = $row['variable'];
		$this->view->group = $row['group'];
		$this->view->description = $row['description'];
		$this->view->msg = '';

		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/general',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function editProc($id) {
		$db = new Database();
		$errors = "null";
		if(isset($_FILES['image'])){
	      $file_name = $this->generateRandomString().".jpg";
	      $file_size = $_FILES['image']['size'];
	      $file_tmp = $_FILES['image']['tmp_name'];
	      $file_type = $_FILES['image']['type'];
	      $file_ext=@strtolower(end(explode('.',$_FILES['image']['name'])));
	      
	      $expensions= array("jpeg","jpg","png");
	      
	      if(in_array($file_ext,$expensions)=== false){
	         $errors="extension not allowed, please choose a JPEG or PNG file.";
	      }
	      
	      if($file_size > 2097152) {
	         $errors='File size must be excately 2 MB';
	      }
	      
	      if($errors=="null") {
	         move_uploaded_file($file_tmp,"public/images/".$file_name);
	         $date    = date("Y-m-d H:i:sa");
	      	 $query = $db->query("UPDATE `settings` SET `value` = '$file_name',`updated_at` = '$date' WHERE variable='$id'");
	         
	      }else{
	        $sth = $this->model->viewPart($id);
			$row=mysqli_fetch_assoc($sth);	
			$this->view->title = "Update ".$row['variable'];
			$this->view->value = $row['value'];
			$this->view->variable = $row['variable'];
			$this->view->group = $row['group'];
			$this->view->description = $row['description'];
			$this->view->msg = '';
			$this->view->msg = $errors;
			$this->view->render('admin/head',$noInclude = true);
			$this->view->render('crud/general',$noInclude = true);
			$this->view->render('admin/foot',$noInclude = true);
	      }
	      

   		} elseif (isset($_POST['description'])) {
   			$sth = $this->model->editValueLink($id);	
			$count =  @mysqli_num_rows($sth);
   		} else {
   			$sth = $this->model->editValue($id);	
			$count =  @mysqli_num_rows($sth);
   		}
   		

		if ($count > 0)	{	   
			$sth = $this->model->viewPart($id);
			$row=mysqli_fetch_assoc($sth);	
			$this->view->title = "Update ".$row['variable'];
			$this->view->value = $row['value'];
			$this->view->variable = $row['variable'];
			$this->view->msg = $errors;
			$this->view->render('admin/head',$noInclude = true);
			$this->view->render('crud/general',$noInclude = true);
			$this->view->render('admin/foot',$noInclude = true);
		} else {
			if (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "general") {
				header('location: '.URL.'settings');
			} elseif (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "social_account") {
				header('location: '.URL.'settings/social');
			} elseif (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "link") {
				header('location: '.URL.'settings/link');
			}
		}
		if(! $sth ) {
	    	die('Could not enter data: ' . mysqli_error($db));
	    }	
		// $this->view->msg = $this->model->msg;	    
	}
	function reset($id) {
		$sth = $this->model->resetValue($id);		
		if (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "general") {
			header('location: '.URL.'settings');
		} else if (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "social_account") {
			header('location: '.URL.'settings/social');
		} elseif (mysqli_fetch_assoc($this->model->viewPart($id))['group'] == "link") {
				header('location: '.URL.'settings/link');
		}
	}

}

//Character Limiter
if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;')
	{
		if (strlen($str) < $n)
		{
			return $str;
		}

		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n)
		{
			return $str;
		}

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';

			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
			}
		}
	}
}
