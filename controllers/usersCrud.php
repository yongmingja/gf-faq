<?php 

class UsersCrud extends Controller {
	
	function __construct(){
		parent::__construct();
		$role = $this->login;
		if ($role == 99) {
			Session::destroy();
			header('location: '.URL.'login');
			exit;
		}
	}

	function index() {
		if ($this->login == 1) {
			header('location: '.URL.'dashboard');
		}
		$this->view->sth = $this->model->run();
		$this->view->msg = '';
		$this->view->title = "Insert User";
		$this->view->name = "";
		$this->view->email = "";
		$this->view->username = "";
		$this->view->level = "";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/usersCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function view() {
		if ($this->login == 1) {
			header('location: '.URL.'dashboard');
		}
		$sth = $this->model->viewCrud($id = false);	
		$this->view->sth = $sth;
		$this->view->title = "User";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/viewUsers',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function delete($id = false) {
		$sth = $this->model->deleteCrud($id);	
		header('location: '.URL.'usersCrud/view');
	}
	function input() {
		if ($this->login == 1) {
			header('location: '.URL.'dashboard');
		}
		$this->view->title = "Insert User";
		$db = new Database();
		$this->view->name = $_POST['name'];
		$this->view->email = $_POST['email'];
		$this->view->username = $_POST['username'];
		$this->view->level = $_POST['level'];
		if (!$_POST['name']) {
			$this->view->msg = 'harap mengisi nama !';
		} else
		if (!$_POST['email']) {
			$this->view->msg = 'harap mengisi email !';
		} else
		if (!$_POST['username']) {
			$this->view->msg = 'harap mengisi username !';
		} else
		if (!$_POST['password']) {
			$this->view->msg = 'harap mengisi password !';
		} else {
		$sth = $this->model->inputCrud();	
		$count =  @mysqli_num_rows($sth);
		if ($count > 0)	{
			$this->view->msg = '<button class="btn btn-danger" disabled>username <i>'.$_POST['username'].'</i> sudah ada</button>';
		} else {
			$this->view->msg = '<button class="btn btn-primary" disabled>Berhasil menambahkan <i>'.$_POST['name'].'</i> ke database</button>';
		}
		if(! $sth ) {
               die('Could not enter data: ' . mysqli_error($db));
            }	
        }
		// $this->view->msg = $this->model->msg;
        $this->view->sth = $this->model->run();
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/usersCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);	
	}
	function edit($id = false) {
		$this->view->sth = $this->model->run();
		$sth = $this->model->viewCrud($id);
		$row=mysqli_fetch_assoc($sth);	
		$this->view->msg = '';
		$this->view->title = "Update User";
		$this->view->id = $row['id'];
		$this->view->name = $row['name'];
		$this->view->email = $row['email'];
		$this->view->username = $row['username'];
		$this->view->level = $row['level'];

		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/editUsers',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function editProc() {
		$db = new Database();
		$this->view->sth = $this->model->run();
		$sth = $this->model->editCrud();	
		$count =  @mysqli_num_rows($sth);
		if ($count > 0 or $sth == 999 )	{
			$this->view->title = "Update User";
			$this->view->id = $_POST['id'];
			$this->view->name = $_POST['name'];
			$this->view->email = $_POST['email'];
			$this->view->username = $_POST['username'];
			$this->view->level = $_POST['level'];
			if($sth == 999){
			$this->view->msg = '<button class="btn btn-danger" disabled>password lama salah!</button>';
			}else {
			$this->view->msg = '<button class="btn btn-danger" disabled>username <i>'.$_POST['username'].'</i> sudah ada</button>';
			}
			$this->view->render('admin/head',$noInclude = true);
			$this->view->render('crud/editUsers',$noInclude = true);
			$this->view->render('admin/foot',$noInclude = true);
		} else {
			header('location: '.URL.'usersCrud/view');
		}
		if(! $sth ) {
	    	die('Could not enter data: ' . mysqli_error($db));
	    }	
		// $this->view->msg = $this->model->msg;	    
	}

}

//Character Limiter
if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;')
	{
		if (strlen($str) < $n)
		{
			return $str;
		}

		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n)
		{
			return $str;
		}

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';

			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
			}
		}
	}
}
