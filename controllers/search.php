<?php 
	
class Search extends Controller{

	function __construct(){
		parent::__construct();
    
	}
	function index() {
    if (isset($_GET['category'])) {
        $sth = $this->model->category();
        $count =  mysqli_num_rows($sth);
        if ($count > 0) {
          $this->view->sth = $this->model->category();;
          $this->view->msg = $_GET['category'];
          $this->view->render('search/category');
        } 
    }
    else if (isset($_GET['id'])) {
        $sth = $this->model->sub();
        $count =  mysqli_num_rows($sth);
        if ($count > 0) {
          $this->view->sth = $this->model->sub();;
          $this->view->msg = $_GET['id'];
          $this->view->db = new Database();
          $this->view->render('search/sub');
        } 
    } else {
      $sth = $this->model->run();
      $count =  @mysqli_num_rows($sth);
      if ($count > 0) {
        $this->view->sth = $this->model->run();;
        $this->view->msg = $_POST['search'];
        $this->view->count = $this->model->countRow();
        $this->view->render('search/index');
      } else {    
        $this->view->msg = $_POST['search'];
        $this->view->render('search/noresult');
      }
    }    	
	}
}
//Character Limiter
if ( ! function_exists('character_limiter'))
{
  function character_limiter($str, $n = 500, $end_char = '&#8230;')
  {
    if (strlen($str) < $n)
    {
      return $str;
    }

    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

    if (strlen($str) <= $n)
    {
      return $str;
    }

    $out = "";
    foreach (explode(' ', trim($str)) as $val)
    {
      $out .= $val.' ';

      if (strlen($out) >= $n)
      {
        $out = trim($out);
        return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
      }
    }
  }
}