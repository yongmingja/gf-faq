<?php

class Help extends Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$this->view->msg = "This is help page";
		$this->view->render('template/index');	

	}

	public function other($arg = false) {
		$this->view->msg = "Other".$arg;
		$this->view->render('template/index');
		
	}

}