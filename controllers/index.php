<?php 

class Index extends Controller{
	function __construct(){
        parent::__construct();       
    	require ('models/index_model.php');
      	$modelName = 'Index_Model';
        $this->model = new $modelName();
        
	}
	function index() {
        
        $newVersion = $this->model->checkVersionDB()['DBVersion'];
        if (!$this->model->sth() or $this->dbVersion != $newVersion){
            header('location: '.URL.'index/setupDB');
			exit;
        }
    	$this->view->sth = $this->model->sth();
    	$this->view->sth2 = $this->model->sth2();
    	$this->view->sth3 = $this->model->sth3();
    	$this->view->sth4 = $this->model->sth4();
		$this->view->render('index/index');
	}
	function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
	}
    function contact() {
        $this->view->title = 'Kontak kami';
        $this->view->msg = '';
        $this->view->fname = '';
        $this->view->lname = '';
        $this->view->email = '';
        $this->view->judul = '';
        $this->view->pesan = '';
        $this->view->captcha = simple_php_captcha();
        $this->view->clientIP = $this->get_client_ip();
        $this->view->render('index/contact');
    }
	function contactProc() {
        $this->view->title = 'Kontak kami';
        $this->view->clientIP = $this->get_client_ip();
        $this->view->fname = $_POST['first_name'];
        $this->view->lname = $_POST['last_name'];
        $this->view->email = $_POST['email'];
        $this->view->judul = $_POST['judul'];
        $this->view->pesan = $_POST['pesan'];
        $this->view->captcha = simple_php_captcha();
	    if (isset($_POST['action'])) {
            if ($_POST['captchaValidate'] !== $_POST['captcha']) {
                $this->view->msg = '    
                            <div id="btnWarning" class="input-field btn col s6" style="color: #b71c1c !important; font-weight: 600 !important" disabled>Wrong captcha</div>';
            } else {
                $contact = $this->model->contact();
                if ($contact == 10 ) {
                    $this->view->msg = '                        
                            <div id="btnWarning" class="input-field btn col s6" style="color: #00695c !important; font-weight: 600 !important" disabled>Success</div>
                            <script type="text/javascript">
                            setTimeout(function(){
                              window.location = "'.URL.'";
                            }, 2000);</script>';
                                    
                }else {
                    $this->view->msg = '    
                            <div id="btnWarning" class="input-field btn col s6" style="color: #b71c1c !important; font-weight: 600 !important" disabled>Failed</div>';
                }
            }
				
		}
	    $this->view->render('index/contact');
	}	
    function view() {
        $this->view->sth = $this->model->contactList();
        $this->view->title = "Daftar pesan";
        $this->view->render('admin/head',$noInclude = true);
        $this->view->render('crud/messageList',$noInclude = true);
        $this->view->render('admin/foot',$noInclude = true);
    }
    function deleteContact($id = false) {
        $this->model->deleteContact($id);
        header('location: '.URL.'index/view');        
    }
    function viewOneContact($id = false){
        $this->view->title = 'Kotak Pesan';
        $sth = $this->model->viewOneContact($id);
        $this->view->sth = mysqli_fetch_assoc($sth);
        $this->view->render('admin/head',$noInclude = true);
        $this->view->render('crud/messageOne',$noInclude = true);
        $this->view->render('admin/foot',$noInclude = true);
    }
	function undermaintenance() {
		$this->view->render('comingsoon/index',$noInclude = true);
	}
    function setupDB() {        
        $link = new Database();
        $row = $this->model->checkVersionDB();
        $versilama = $row['DBVersion'];
        $filename = "setup.sql";

        if ( $versilama >= $this->dbVersion ) {
            echo '<script type="text/javascript">
                            setTimeout(function(){
                              window.location = "'.URL.'";
                            }, 2000);</script>';
            die( "Your database already up to date!<br>" . "DB Version: " . $row['DBVersion'] . "   SetupDB Version: " . $this->dbVersion . "  <br>redirecting to home...");
            
        }

        set_time_limit(0);
        ini_set('memory_limit', -1 );
        ini_set('max_execution_time', 60 * 60 * 24 * 1);        
        //$login = new gfsoftlogin(1,false);

        if ( isset($filename) ) {

            $templine = '';
            if (file_exists($filename)){
                $lines = file($filename);		
                foreach ($lines as $line){
                    if (substr($line, 0, 2) == '--' || $line == '')
                        continue;
                
                    $templine .= $line;
                    if (substr(trim($line), -1, 1) == ';'){
                        $link->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $this->model->errorDB() . '<br /><br />');
                        $templine = '';
                    }
                }
                $query = 'UPDATE `tblcontrol` SET `DBVersion` = '.DB_VERSION.' WHERE `tblcontrol`.`id` = 1';
                $db = new Database(); $db->query($query);
                echo '<script type="text/javascript">
                            setTimeout(function(){
                              window.location = "'.URL.'";
                            }, 2000);</script>';
                
                
                die('Success <br> redirecting to home...');
                
            }
            

            // if ( strpos($_SERVER['HTTP_HOST'],"localhost") === false ) {
                // $command = "gunzip < backup/$filename | mysql -u " . DATABASE_USER . " -p" . DATABASE_PASS ." ".DATABASE_NAME; //username dan password database
            // } else {
                // $curdir = getcwd();
                // $htdocspos = strpos($curdir,'htdocs');
                // $command = substr($curdir,0,$htdocspos) . "mysql/bin/mysql -u root " . DATABASE_NAME . " < backup/$filename";
            // }

            // echo system($command);

            

        }

        die('Failed process! or file not found!');
        
    }
}