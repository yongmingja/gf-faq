<?php 
	
class Category extends Controller{

	function __construct(){
		parent::__construct();
	}
	function index() {
		$query = $this->model->viewCatParent($_GET['id']);
		$db = new Database();
		$this->view->db = $db;
		$this->view->row = mysqli_fetch_assoc($query);
		$this->view->sth = $this->model->viewCat($_GET['id']);
		$this->view->render('category/category');
	}
	function sub($id) {
		$this->view->sth = $this->model->viewContent($id);
		$this->view->row = mysqli_fetch_assoc($this->model->viewContent($id));
		$this->view->render('category/sub');
	}

}