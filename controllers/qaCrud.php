<?php 
class QaCrud extends Controller {
	
	function __construct(){
		parent::__construct();
		$role = $this->login;
		if ($role == 99) {
			Session::destroy();
			header('location: '.URL.'login');
			exit;
		}
	}

	function index() {
		$this->view->sth = $this->model->run();
		$this->view->msg = '';
		$this->view->question = '';
		$this->view->answer = '';
		$this->view->title = "Insert Q&A";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/qaCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function view() {
		$sth = $this->model->viewCrud($id = false);	
		$this->view->sth = $sth;
		$this->view->title = "Q&A";
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/viewQA',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function delete($id = false) {
		$sth = $this->model->deleteCrud($id);	
		header('location: '.URL.'qaCrud/view');
	}
	function input() {
		$this->view->title = "Insert User";
		$db = new Database();
		$this->view->question = $_POST['question'];
		$this->view->answer = $_POST['answer'];
		$this->view->category = $_POST['category'];
		if (!$_POST['question']) {
			$this->view->msg = 'harap mengisi pertanyaan !';
		} else
		if (!$_POST['answer']) {
			$this->view->msg = 'harap mengisi jawaban !';
		} else
		if (!$_POST['category']) {
			$this->view->msg = 'harap mengisi category !';
		} else {
		$sth = $this->model->inputCrud();	
		$count =  @mysqli_num_rows($sth);
		if ($count > 0)	{
			$this->view->msg = '<button class="btn btn-danger" disabled>question <i>'.$_POST['question'].'</i> sudah ada</button>';
		} else {
			$this->view->msg = '<button class="btn btn-primary" disabled>Berhasil menambahkan <i>'.$_POST['question'].'</i> ke database</button>';
		}
		if(! $sth ) {
               die('Could not enter data: ' . mysqli_error($db));
            }	
        }
		// $this->view->msg = $this->model->msg;
        $this->view->sth = $this->model->run();
		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/qaCrud',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);	
	}
	function edit($id = false) {
		$this->view->sth = $this->model->run();
		$sth = $this->model->viewCrud($id);
		$row=mysqli_fetch_assoc($sth);	
		$this->view->msg = '';
		$this->view->title = "Update Q&A";
		$this->view->id = $row['id'];
		$this->view->question = $row['question'];
		$this->view->answer = $row['answer'];
		$this->view->category = $row['category'];

		$this->view->render('admin/head',$noInclude = true);
		$this->view->render('crud/editQA',$noInclude = true);
		$this->view->render('admin/foot',$noInclude = true);
	}
	function editProc() {
		$db = new Database();
		$this->view->sth = $this->model->run();
		$sth = $this->model->editCrud();	
		$count =  @mysqli_num_rows($sth);
		if ($count > 0)	{
			$this->view->title = "Update Q&A";
			$this->view->id = $_POST['id'];
			$this->view->question = $_POST['question'];
			$this->view->category = $_POST['category'];
			$this->view->answer = $_POST['answer'];
			$this->view->msg = '<button class="btn btn-danger" disabled><i>'.$_POST['question'].'</i> sudah ada</button>';
			$this->view->render('admin/head',$noInclude = true);
			$this->view->render('crud/editQA',$noInclude = true);
			$this->view->render('admin/foot',$noInclude = true);
		} else {
			header('location: '.URL.'qaCrud/view');
		}
		if(! $sth ) {
	    	die('Could not enter data: ' . mysqli_error($db));
	    }	
		// $this->view->msg = $this->model->msg;	    
	}

}

//Character Limiter
if ( ! function_exists('character_limiter'))
{
	function character_limiter($str, $n = 500, $end_char = '&#8230;')
	{
		if (strlen($str) < $n)
		{
			return $str;
		}

		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

		if (strlen($str) <= $n)
		{
			return $str;
		}

		$out = "";
		foreach (explode(' ', trim($str)) as $val)
		{
			$out .= $val.' ';

			if (strlen($out) >= $n)
			{
				$out = trim($out);
				return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
			}
		}
	}
}
