<?php 
	
class Errors extends Controller{

	function __construct(){
		parent::__construct();
		
	}
	function index() {
		$this->view->msg = '<font color="red">The page that you have requested could not be found.</font>';
		$this->view->render('error/index');
	}
}