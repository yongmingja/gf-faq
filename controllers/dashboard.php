<?php

class Dashboard extends Controller {

	function __construct() {
		parent::__construct();
		$role = $this->login;
		if ($role == 99) {
			Session::destroy();
			header('location: '.URL.'login');
			exit;
		}
	}
	
	function index() 
	{	
		$newVersion = $this->model->checkVersionDB()['DBVersion'];
        if (!$this->model->sth() or $this->dbVersion > $newVersion){
            header('location: '.URL.'index/setupDB');
			exit;
        }
		$msg = $this->model->viewContact();
		$this->view->msg = $msg;
		$count =  mysqli_num_rows($msg);
		$this->view->contact = $count;
		$qa = $this->model->viewQa();	
		$count =  mysqli_num_rows($qa);
		$this->view->cQa = $count;
		$category = $this->model->viewCategory();
		$count =  mysqli_num_rows($category);
		$this->view->cCat = $count;
		$user = $this->model->viewUsers();
		$count =  mysqli_num_rows($user);
		$this->view->cUsr = $count;
		$this->view->title = "Dashboard";
	    $this->view->render('admin/head',$noInclude = true);
	    $this->view->render('admin/dashboard',$noInclude = true);
	    $this->view->render('admin/foot',$noInclude = true);
	}
	
	function logout()
	{
		Session::destroy();
		header('location: '.URL);
		exit;
	}

	function showBackUpDB() {
		$this->view->title = "Backup &amp; Restore";
		$this->view->sth = scandir('backup/',1);		
		$this->view->render('admin/head',$noInclude = true);
	    $this->view->render('admin/showdb',$noInclude = true);
	    $this->view->render('admin/foot',$noInclude = true);
	}
	function deleteDb($filename){
		unlink('././backup/'.$filename);
		header('location: ../showBackUpDB');
	}
	function backup_tables($nama_file,$tables ='*')	{
		set_time_limit(0);
		ini_set('memory_limit', -1 );
		ini_set('max_execution_time', 60 * 60 * 24 * 1);

		if (is_dir("./backup") !== true){
			mkdir("./backup", 0755);
		}
		$link = new Database();
		$return = '';
		
		if($tables == '*'){
			$tables = array();
			$result = $link->query('SHOW TABLES');
			while($row = mysqli_fetch_row($result)){
				$tables[] = $row[0];
			}
		}
		else{//jika hanya table-table tertentu
			$tables = is_array($tables) ? $tables : explode(',',$tables);
		}
		
		foreach($tables as $table){
			$result = $link->query('SELECT * FROM '.$table);
			$num_fields = mysqli_num_fields($result);

			$return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
			$row2 = mysqli_fetch_row($link->query('SHOW CREATE TABLE '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) {
				while($row = mysqli_fetch_row($result)){
					// $row = addslashes($row[0]);
					// $row = str_replace("\n","\\n",$row);
					// echo "<pre>"; print $row; echo "</pre>";
					//menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j<$num_fields; $j++) {
						//akan menelusuri setiap baris query didalam
						if ( is_null($row[$j]) ) { 
							$return.= 'NULL'; 
						} else 
						if ( $row[$j] == '' ) { 
							$return.= '""'; 
						} else 
						if (isset($row[$j])) { 
							$row[$j] = addslashes($row[$j]);
							$row[$j] = str_replace("\n","\\n",$row[$j]);
							$return.= '"'.$row[$j].'"' ; 
						}
						if ($j<($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
				}
			}
			$return.="\n\n\n";
		}							
		//simpan file di folder
		$nama_file;
		
		$handle = fopen('backup/'.$nama_file,'w+');
		fwrite($handle,$return);
		fclose($handle);
	}
	function runBackUp() {
		$backupFile = 'gffaq' . date("Ymd") . '.sql';
		$this->backup_tables($backupFile);

		// $perintahdb = "UPDATE tblcontrol SET BackupDataDateTime='" . date('Y-m-d H:i:s') . "'";
		// $sqlquery = @mysql_query( $perintahdb );

		$_SESSION['filedownload'] = 'backup/' . $backupFile;
		header('location: showBackUpDB');
	}
	
	function runRestore($filename) {
		set_time_limit(0);
		ini_set('memory_limit', -1 );
		ini_set('max_execution_time', 60 * 60 * 24 * 1);
		$link = new Database();
		//$login = new gfsoftlogin(1,false);

		if ( isset($filename) ) {

			$templine = '';
			if (file_exists('backup/'. $filename)){
				$lines = file('backup/'. $filename);		
				foreach ($lines as $line){
					if (substr($line, 0, 2) == '--' || $line == '')
						continue;
				
					$templine .= $line;
					if (substr(trim($line), -1, 1) == ';'){
						$link->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $link->error() . '<br /><br />');
						$templine = '';
					}
				}
				die('Restore Success');
			}
			

			// if ( strpos($_SERVER['HTTP_HOST'],"localhost") === false ) {
				// $command = "gunzip < backup/$filename | mysql -u " . DATABASE_USER . " -p" . DATABASE_PASS ." ".DATABASE_NAME; //username dan password database
			// } else {
				// $curdir = getcwd();
				// $htdocspos = strpos($curdir,'htdocs');
				// $command = substr($curdir,0,$htdocspos) . "mysql/bin/mysql -u root " . DATABASE_NAME . " < backup/$filename";
			// }

			// echo system($command);

			

		}

		die('Failed process! or file not found!');
	}

}