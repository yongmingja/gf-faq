<?php 
//Autoload
require ('libs/Bootstrap.php');
require ('libs/Controller.php');
require ('libs/Model.php');
require ('libs/View.php');
require ('libs/captcha/simple-php-captcha.php');

//libs
require ('libs/Session.php');
require ('libs/Database.php');

require ('config/paths.php');
require ('config/database.php');
$app = new Bootstrap();