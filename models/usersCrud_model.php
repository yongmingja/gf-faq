<?php

class UsersCrud_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	public function run() {
		$query = "SELECT * FROM `access_level`";
		$sth = $this->db->query($query);
		return $sth;
	}

	public function viewCrud($id) {
		if (!$id) {
			$query = "SELECT * FROM `users` order by id desc";
			$sth = $this->db->query($query);
			return $sth;
		} else {
			$this->id = "WHERE password = '$id'";
			$query = "SELECT * FROM `users`".$this->id;
			$sth = $this->db->query($query);
			return $sth;
		}
	}

	public function inputCrud() {
		$username	= $_POST['username'];
		$password 	= md5($_POST['password']);
		$name 		= $_POST['name'];
		$email 		= $_POST['email'];
		$level 		= $_POST['level'];
		$date		= date("Y-m-d H:i:s");
		$query 		= "SELECT * FROM `users` WHERE username='$username'";
		$sth 		= $this->db->query($query);
		$count 		=  mysqli_num_rows($sth);
        if ($count > 0) {
          	return $sth;
        } else {
        	$query 	= '	INSERT INTO `users` (id, name, email, username, password, level, Created) 
        				VALUES (NULL, "'.$name.'","'.$email.'","'.$username.'","'.$password.'","'.$level.'","'.$date.'")';
			$sth 	= $this->db->query($query);
        	return $sth;
        }		
		
	}
	public function editCrud() {

        $username	= $_POST['username'];
		$password 	= md5($_POST['password']);
		$oldPassword= md5($_POST['oldPassword']);
		$name 		= $_POST['name'];
		$email 		= $_POST['email'];
		$level 		= $_POST['level'];
		$date		= date("Y-m-d H:i:s");
		$id 		= $_POST['id'];
		$query 		= "SELECT * FROM `users` WHERE username='$username'"; //query for check username not be same
		$sth 		= $this->db->query($query);
		$count 		= mysqli_num_rows($sth);
		$query 		= "SELECT * FROM `users` WHERE id='$id'"; //query for no change username
		$sth 		= $this->db->query($query);
		$row 		= mysqli_fetch_assoc($sth);
		if ($oldPassword != $row['password']){
			//old password doesn't match
			return $sth = 999;
		}elseif ($row['username'] == $username) { //check if same username(not changed) = true
			$query 	= "	UPDATE `users` SET `name` = '$name', `email` = '$email', `username` = '$username', `password` = '$password', `level` = $level, `Modified` = '$date' WHERE `users`.`id`=$id ";
			$sth 	= $this->db->query($query);
        	return $sth;
		}elseif ($count > 0) { //check if username available = true
          	return $sth;
        } else {
        	$query 	= "	UPDATE `users` SET `name` = '$name', `email` = '$email', `username` = '$username', `password` = '$password', `level` = $level, `Modified` = '$date' WHERE `users`.`id`=$id ";
			$sth 	= $this->db->query($query);
        	return $sth;
        }	
		
	}
	public function deleteCrud($id) {
		$query = "DELETE FROM `users` WHERE id=$id";
		$this->db->query($query);
		return $id;
	}

	
}