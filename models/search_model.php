<?php

class Search_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function countRow() {
		$search = $this->db->real_escape_string($_POST['search']);
		$query = "SELECT * FROM `qa` WHERE question LIKE '%$search%' or `answer` LIKE '%$search%'";
		$sth = $this->db->query($query);
		$total = mysqli_num_rows($sth);		
		return $total;
	}
	public function run() {
		$search = $this->db->real_escape_string($_POST['search']);
		$total = $this->countRow();
		$limit = 10;
		$pages = ceil($total / $limit);

	    // What page are we currently on?
	    $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
	        'options' => array(
	            'default'   => 1,
	            'min_range' => 1,
	        ),
	    )));
	    $offset = ($page - 1)  * $limit;
	    $query = "SELECT * FROM `qa` WHERE `question` LIKE '%$search%' or `answer` LIKE '%$search%' ORDER BY question LIMIT $limit OFFSET $offset";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function category() {
		$id = $_GET['category'];
		$query = "SELECT * FROM `qa` WHERE category='$id'";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function sub() {
		error_reporting(0);
		$id = $_GET['id'];
		$query = "SELECT * FROM `qa` WHERE id=$id" ;
		$sth2 = $this->db->query($query);
		// $row = mysqli_fetch_assoc($sth);
		// echo $row['views'];
		// echo $row['question'];
		$count = 0;
		foreach ($sth2 as $s) {
			$count = $s['views'];
		}
		// echo $count;
		
		$count = $count + 1;
		$query =  "UPDATE `qa` SET `views` = $count WHERE id=$id";
		$sth3 = $this->db->query($query);
		mysqli_close($this->db);//kill database for not counting twice
		$db = new Database();
		$query = "SELECT * FROM `qa` WHERE id=$id";
		$sth = $db->query($query);
		return $sth;
	}
	
}

