<?php

class CategoryCrud_Model extends Model
{
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	public function viewCrud2() {
			$query = "SELECT * FROM `category`";
			$sth = $this->db->query($query);
			return $sth;		
	}
	public function viewCrud3() {
			$query = "SELECT * FROM `category`";
			$sth = $this->db->query($query);
			return $sth;		
	}

	public function viewCrud($id) {
		if (!$id) {
			$query = "SELECT * FROM `category` order by id desc";
			$sth = $this->db->query($query);
			return $sth;
		} else {
			$this->id = "WHERE id = $id";
			$query = "SELECT * FROM `category`".$this->id;
			$sth = $this->db->query($query);
			return $sth;
		}
	}

	public function inputCrud() {
		$cName = $_POST['cName'];
		$parent = $_POST['parent'];
		$created_by = Session::get('userID');
		$date	= date("Y-m-d H:i:s");
		$query = "SELECT * FROM `category` WHERE cName='$cName'";
		$sth = $this->db->query($query);
		$count =  mysqli_num_rows($sth);
        if ($count > 0) {
          	return $sth;
        } else {
        	$query = 'INSERT INTO `category` (id, cName, parent, created_by, Created) VALUES (NULL, "'.$cName.'",'.$parent.',"'.$created_by.'","'.$date.'")';
			$sth = $this->db->query($query);
			$msg = "berhasil";			
        	return $sth;
        }
	}
	public function editCrud() {
		$cName 		= $_POST['cName'];
		$id 		= $_POST['id'];
		$parent 	= $_POST['parent'];
		$modified_by = Session::get('userID');
		$date		= date("Y-m-d H:i:s");
		$query 		= "SELECT * FROM `category` WHERE cName='$cName'";//query for check cName not be same
		$sth 		= $this->db->query($query);
		$count 		=  mysqli_num_rows($sth);
		$query 		= "SELECT * FROM `category` WHERE id='$id'"; //query for no change cName
		$sth 		= $this->db->query($query);
		$row 		= mysqli_fetch_assoc($sth);
		if ($row['cName'] == $cName) { //check if same cName(not changed) = true
			$query = "UPDATE `category` SET `cName` = '$cName',`parent` = '$parent', `modified_by` = '$modified_by', `Modified` = '$date' WHERE id=$id";
			$sth = $this->db->query($query);
			return $sth;
		}else if ($count > 0) { //check if cName available = true
          	return $sth;
        } else {
			$query = "UPDATE `category` SET `cName` = '$cName',`parent` = '$parent', `modified_by` = '$modified_by', `Modified` = '$date' WHERE id=$id";
			$sth = $this->db->query($query);
			return $sth;
        }

		
	}
	public function deleteCrud($id) {
		$query = "DELETE FROM `category` WHERE id=$id";
		$this->db->query($query);
		return $id;
	}
	
}