<?php

class Settings_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function viewGeneral() {
		$query = "SELECT * FROM `settings` WHERE `group`='general' ORDER BY `variable` ASC";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewSocial() {
		$query = "SELECT * FROM `settings` WHERE `group`='social_account' ORDER BY `variable` ASC";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewLink() {
		$query = "SELECT * FROM `settings` WHERE `group`='link' ORDER BY `variable` ASC";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewPart($id) {
		$query = "SELECT * FROM `settings` WHERE `variable`='$id'";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function editValue($id) {
		$file_name = $_POST['value'];
		$date    = date("Y-m-d H:i:s");
      	$query = $this->db->query("UPDATE `settings` SET `value` = '$file_name',`updated_at` = '$date' WHERE variable='$id'");
	}
	public function editValueLink($id) {
		$file_name = $_POST['value'];
		$description = $_POST['description'];
		$date    = date("Y-m-d H:i:s");
      	$query = $this->db->query("UPDATE `settings` SET `value` = '$file_name',`description` = '$description',`updated_at` = '$date' WHERE variable='$id'");
	}
	public function resetValue($id) {
		$sth = $this->viewPart($id);
		$row = mysqli_fetch_assoc($sth);
		$file_name = $row['default'];
		$date    = date("Y-m-d H:i:s");
      	$query = $this->db->query("UPDATE `settings` SET `value` = '$file_name',`updated_at` = '$date' WHERE variable='$id'");
	}

}