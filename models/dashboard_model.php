<?php 

class Dashboard_Model extends Model {
	function __construct() {
		parent::__construct();
	}
	public function checkVersionDB() {
		$query = "SELECT DBVersion FROM tblcontrol";
		$sth = $this->db->query($query);		
		return @mysqli_fetch_assoc($sth);
	}
	public function sth() {
		$query = "SELECT * FROM `category`";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewQa() {
		$query = "SELECT * FROM `qa`";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewCategory() {
		$query = "SELECT * FROM `category`";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewUsers() {
		$query = "SELECT * FROM `users`";
		$sth = $this->db->query($query);
		return $sth;
	}
	public function viewContact(){
		$query = "SELECT * FROM `contact`";
		$sth = $this->db->query($query);
		return $sth;
	}
}