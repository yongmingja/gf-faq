<?php

class QaCrud_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	public function run() {
		$query = "SELECT * FROM `category`";
		$sth = $this->db->query($query);
		return $sth;
	}

	public function viewCrud($id) {
		if (!$id) {
			$query = "SELECT * FROM `qa` order by id desc";
			$sth = $this->db->query($query);
			return $sth;
		} else {
			$this->id = "WHERE id = $id";
			$query = "SELECT * FROM `qa`".$this->id;
			$sth = $this->db->query($query);
			return $sth;
		}
	}

	public function inputCrud() {
		$question = $_POST['question'];
		$answer = addslashes($_POST['answer']);
		$category = $_POST['category'];
		$created_by = Session::get('userID');
		$date		= date("Y-m-d H:i:s");
		$query = "SELECT * FROM `qa` WHERE question='$question'";
		$sth = $this->db->query($query);
		$count =  mysqli_num_rows($sth);
        if ($count > 0) {
          	return $sth;
        } else {
        	$query = 'INSERT INTO `qa` (id, category, question, answer, created_by, Created) VALUES (NULL, "'.$category.'","'.$question.'","'.$answer.'","'.$created_by.'","'.$date.'")';
			$sth = $this->db->query($query);
			$msg = "berhasil";			
        	return $sth;
        }		
		
	}
	public function editCrud() {
		$question 	= $_POST['question'];
		$answer 	= $_POST['answer'];
		$modified_by = Session::get('userID');
		$category 	= $_POST['category'];
		$id 		= $_POST['id'];
		$date		= date("Y-m-d H:i:s");
		$query = "SELECT * FROM `qa` WHERE question='$question'";//query for check question not be same
		$sth = $this->db->query($query);
		$count =  mysqli_num_rows($sth);
		$query 		= "SELECT * FROM `qa` WHERE id='$id'"; //query for no change question
		$sth 		= $this->db->query($query);
		$row = mysqli_fetch_assoc($sth);
		if ($row['question'] == $question) { //check if same question(not changed) = true
			$query = "UPDATE `qa` SET `question` = '$question',`answer` = '$answer',`category` = '$category', `modified_by` = '$modified_by', `Modified` = '$date' WHERE id=$id";
			$sth = $this->db->query($query);
			return $sth;
		}else if ($count > 0) { //check if username available = true
          	return $sth;
        } else {
			$query = "UPDATE `qa` SET `question` = '$question',`answer` = '$answer',`category` = '$category', `modified_by` = '$modified_by', `Modified` = '$date' WHERE id=$id";
			$sth = $this->db->query($query);
			return $sth;
        }
		
	}

	public function deleteCrud($id) {
		$query = "DELETE FROM `qa` WHERE id=$id";
		$this->db->query($query);
		return $id;
	}

	
}