<?php 

class View {
	
	function __construct()	{
	}

	public function render($name, $noInclude = false) {
		
		if ($noInclude == true) {
			require 'views/' . $name . '.php'; 
		} else {
			require 'views/template/head.php';
			require 'views/' . $name . '.php'; 
			require 'views/template/foot.php';
		}
	}

}