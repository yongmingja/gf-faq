<?php

class Controller {

	function __construct() 
	{
		//echo 'Main controller<br />';	
		date_default_timezone_set('Asia/Jakarta');
		$this->login = $this->fetch_role();		
		$this->view = new View();
		$this->view->dbc = new Database();
		$this->view->role = $this->fetch_role();
		$this->dbVersion = DB_VERSION; 

	}
	
	public function loadModel($name) 
	{
		if ($name === 'index'){
			$name = "";
		}
		$path = 'models/'.$name.'_model.php';
		
		if (file_exists($path)) {
			require 'models/'.$name.'_model.php';
			
			$modelName = $name . '_Model';
			$this->model = new $modelName();
		}		
	}
	function fetch_role() {
    $role = 99;
    Session::init();
    $session = Session::get('userID');
    if(isset($session))
    {
        // User exists
        $db = new Database();
        $sql = "SELECT * FROM users WHERE id=$session LIMIT 1";

        // RUN THE MYSQL QUERY TO FETCH THE USER, SAVE INTO $row
		$sth = $db->query($sql);
		$row = mysqli_fetch_assoc($sth);
        if(!empty($row))
        {
            $role = $row['level'];
        }
    }

    return $role;
	}

}