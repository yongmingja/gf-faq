<?php

class Database extends mysqli
{
	
    public function __construct() {
        $query = "CREATE DATABASE IF NOT EXISTS " . DB_NAME . " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
        mysqli_query(mysqli_connect(DB_HOST,DB_USER, DB_PASS),$query);
        parent::__construct(DB_HOST,DB_USER, DB_PASS,DB_NAME);

        if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }
    }
	
}