<script type="text/javascript">
	function setValue() {
		document.getElementById("search").value="";
	}
</script>
<br>
<style type="text/css">
  p {

  }
</style>
  <nav class="container nav-wrapper" style="box-shadow: none; background-color:#fff;">
  <div class="row">
  <div class="col s7">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="<?php echo URL ?>" class="breadcrumb">GF-Help</a>
        <span class="breadcrumb" style="color: #627b8fb3  ;"><?php echo $this->msg; ?></span>
        </div>
    </div>
  </div>
  <div class="col s5">
    <nav class="searchBar">
        <div class="nav-wrapper">
          <form method="post" action='<?php echo URL ?>search'>
            <div class="input-field">
              <input id="search" name="search" type="search" value="<?php echo $this->msg; ?>" required>
              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
              <i class="material-icons"  onclick="setValue()">close</i>
            </div>
          </form>
        </div>
    </nav>
  </div>
  </div>
  </nav>
<br><br><br><br>
<?php
    $sResult = $this->msg;
    $limit = 10;
    $total = $this->count;
    // How many pages will there be
    $pages = ceil($total / $limit);

    // What page are we currently on?
    $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
        'options' => array(
            'default'   => 1,
            'min_range' => 1,
        ),
    )));
    // Calculate the offset for the query
    $offset = ($page - 1)  * $limit;

    // Some information to display to the user
    $start = $offset + 1;
    $end = min(($offset + $limit), $total);

    // The "back" link
    $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

    // The "forward" link
    $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

    // Display the paging information
    $pagination = '<div class="row setting3"><p>'.$prevlink. ' Page '.$page. ' of '.$pages. ' pages, displaying '.$start. '-'.$end. ' of '.$total. ' results '.$nextlink. ' </p></div>';

?>

<style type="text/css">
  a.disabled {
   pointer-events: none;
   cursor: default;
   color:grey;
}
</style>
<script type="text/javascript">
  var page = <?php echo $page;?>;
  var pages = <?php echo $pages;?>;
  if (page != 1) {
    $(function() {
      $('#sc1').removeClass('disabled');
      $('#sc2').removeClass('disabled');
    });
  } if(page == pages) {
    $(function() {
      $('#sc3').addClass('disabled');
      $('#sc4').addClass('disabled');
    });
  }
</script>
<div class="container backcover2">
  <div class="section" style="padding-top: 0;">
    <div class="row">
      <div class="col s12 setting2"><span class="">Search result</span><br>
      <span class="subSetting2"><?php echo $this->count; ?> result for "<?php echo $sResult; ?>"</span>
      </div>
    </div>
  </div>
  <div class="row center" style="font-size: 150%">
  <p>
    <A HREF="javascript:document.submitForm1.submit()" id="sc1" class="disabled">&laquo;</A>
    <A HREF="javascript:document.submitForm2.submit()" id="sc2" class="disabled">&lsaquo;</A>
     Page <?php echo $page; ?> of <?php echo $pages; ?> pages, displaying <?php echo $start; ?>-<?php echo $end; ?> of <?php echo $total; ?> results
    <A HREF="javascript:document.submitForm3.submit()" id="sc3">&rsaquo;</A>
    <A HREF="javascript:document.submitForm4.submit()" id="sc4">&raquo;</A>
  </p>
  </div>
<?php foreach ($this->sth as $b){ ?>
  <?php $str = $b['answer'];?>
    <div class="row setting3">          
      <div class="col s11"><a href="search?id=<?php echo $b['id'];?>"><?php echo character_limiter($b['question'], 60);?></a></div>      
      <div class="light col s11 subSetting3" style="white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
max-width: 1220px;
max-height: 40px;"><?php echo $str;?></div>
    </div>
<?php } ?>          
    <br><br><br><br><br>

<div class="row center" style="font-size: 150%">
<p>
  <A HREF="javascript:document.submitForm1.submit()" id="sc1" class="disabled">&laquo;</A>
  <A HREF="javascript:document.submitForm2.submit()" id="sc2" class="disabled">&lsaquo;</A>
   Page <?php echo $page; ?> of <?php echo $pages; ?> pages, displaying <?php echo $start; ?>-<?php echo $end; ?> of <?php echo $total; ?> results
  <A HREF="javascript:document.submitForm3.submit()" id="sc3">&rsaquo;</A>
  <A HREF="javascript:document.submitForm4.submit()" id="sc4">&raquo;</A>
</p>
</div>


<form name="submitForm1" method="POST" action="?page=1">
<input type="hidden" name="search" value="<?php echo $this->msg;?>">
</form>
<form name="submitForm2" method="POST" action="?page=<?php echo $page-1;?>">
<input type="hidden" name="search" value="<?php echo $this->msg;?>">
</form>
<form name="submitForm3" method="POST" action="?page=<?php echo $page+1;?>">
<input type="hidden" name="search" value="<?php echo $this->msg;?>">
</form>
<form name="submitForm4" method="POST" action="?page=<?php echo $pages;?>">
<input type="hidden" name="search" value="<?php echo $this->msg;?>">
</form>
</div>
