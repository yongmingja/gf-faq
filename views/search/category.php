<script type="text/javascript">
	function setValue() {
		document.getElementById("search").value="";
	}
</script>
<br>
  <nav class="container nav-wrapper" style="box-shadow: none; background-color:#fff;">
  <div class="row">
  <div class="col s7">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="<?php echo URL ?>" class="breadcrumb">GF-Help</a>
        <span class="breadcrumb" style="color: #627b8fb3  ;"><?php echo $this->msg; ?></span>
        </div>
    </div>
  </div>
  <div class="col s5">
    <nav class="searchBar">
        <div class="nav-wrapper">
          <form method="post" action='<?php echo URL ?>search'>
            <div class="input-field">
              <input id="search" name="search" type="search" required>
              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
              <i class="material-icons"  onclick="setValue()">close</i>
            </div>
          </form>
        </div>
    </nav>
  </div>
  </div>
  </nav>
<br><br><br><br>

<div class="container backcover2">
  <div class="section" style="padding-top: 0;">
    <div class="row">
      <div class="col s12 setting2"><span class="">Search result</span><br>
      </div>
    </div>
  </div>
<?php foreach ($this->sth as $b){ ?>
    <div class="row setting3">          
      <div class="col s11"><a href="search?id=<?php echo $b['id'];?>"><?php echo $b['question'];?></a></div>
      <p class="light col s11 subSetting3"><?php echo $b['answer'];?></p>
    </div>
<?php } ?>          
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>