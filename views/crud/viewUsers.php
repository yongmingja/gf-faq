<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar <?php echo $this->title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar <?php echo $this->title;?></li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
    <a href="./" class="btn btn-primary">Add Content</a>
    <br />
    <br />
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Level</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php 
$no = 1;
foreach ($this->sth as $b){ 
?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $b['name']; ?></td>
                  <td><?php echo $b['username']; ?></td>
                  <td><?php echo $b['email']; ?></td>
                  <td><?php echo $b['level']; ?></td>
                  <td>
                      <a href="edit/<?php echo $b['password'];?>" title="Edit">
                          <span class="label label-success"><i class="fa fa-pencil-square-o"></i></span>
                      </a>
                      <?php if ($b['level'] == 0) {?>
                      <a disabled title="Cannot Delete Admin">
                          <span class="label" style="color:gray;"><i class="fa fa-trash-o"></i></span>
                      </a>
                      <?php }else{?>
                      <a onclick="return confirm('Are You Sure ?')" href="delete/<?php echo $b['id'];?>" title="Delete">
                          <span class="label label-danger"><i class="fa fa-trash-o"></i></span>
                      </a>
                      <?php }?>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
