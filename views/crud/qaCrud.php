<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

tinymce.init({
      selector: "#post_content",
      theme: 'modern',
      paste_data_images:true,
      relative_urls: false,
      remove_script_host: false,
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview forecolor backcolor emoticons",
      image_advtab: true,
      plugins: [
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen",
         "insertdatetime nonbreaking save table contextmenu directionality",
         "emoticons template paste textcolor colorpicker textpattern"
      ],
      automatic_uploads: true,
      //images_upload_path: '<?php echo URL;?>uploads/images/qa',
      images_upload_url: '<?php echo URL;?>image/blog/posts/tinymce_upload_handler',
      file_picker_types: 'image', 
      
      file_picker_callback: function(cb, value, meta) {
         var input = document.createElement('input');
         input.setAttribute('type', 'file');
         input.setAttribute('accept', 'image/*');
         input.onchange = function() {
            var file = this.files[0];
            console.log(file);
            var reader = new FileReader();            
            reader.readAsDataURL(file);
            reader.onload = function () {
               var id = 'post-image-' + (new Date()).getTime();
               var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
               var blobInfo = blobCache.create(id, file, reader.result);
               blobCache.add(blobInfo);
               cb(blobInfo.blobUri(), { title: file.name });
               console.log(reader.result);
            };
          console.log(reader);
          console.log(reader.result);
         };
         input.click();
         

      }
   });

</script>

		  	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Q&amp;A
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Input Q&amp;A</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
            <!-- form start -->
            <form role="form" method="post" action="<?php echo URL ?>qaCrud/input">
              <div class="box-body">
                <div class="form-group">
                  <label for="question">Question</label>
                  <input type="text" name="question" class="form-control" id="question" placeholder="Enter question" value="<?php echo $this->question;?>">
                </div>
                <div class="form-group">
                  <label>Answer</label>
                  <textarea class="form-control" id="post_content" name="answer" rows="3" placeholder="Enter the answer..."><?php echo $this->answer;?></textarea>
                </div>
                <style>select option:disabled {color: #171616;font-weight: 600;}</style>
                <div class="form-group">
                  <label>Category</label>
                  <select name="category" class="form-control">                    
                  <?php foreach ($this->sth as $b){ 
                    if ($b['parent'] == 0){ 
                      ?>
                  <option value="<?php echo $b['cName'];?>" disabled><?php echo $b['cName'];?></option>
                        <?php
                     }                    
                      elseif ($this->category == $b['cName']){
                        ?>
                  <option value="<?php echo $b['cName'];?>" selected><?php echo $b['cName'];?></option>
                        <?php
                      }
                      elseif ($this->category != $b['cName']){
                        ?>
                  <option value="<?php echo $b['cName'];?>"><?php echo $b['cName'];?></option>
                        <?php 
                      }                  
                   } ?> 
                  </select>
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">
                <button type="submit" class="btn btn-success">Input</button>
                <a href="<?php echo URL ?>qaCrud/view" class="btn btn-primary">Cancel</a>
                <?php echo $this->msg; ?>
              </div>
            </form>
          </div>
		  

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->