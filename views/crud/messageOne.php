<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->title;?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">      
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><b><?php echo $this->sth['judul'];?></b></h3>
                <h5>From: <?php echo $this->sth['first_name'].' '.$this->sth['last_name'].'('.$this->sth['email'].')'; ?>)
                  <span class="mailbox-read-time pull-right"><?php echo $this->sth['date_sent'];?></span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                <div class="btn-group">
                  <a href="../../index/deleteContact/<?php echo $this->sth['id'];?>" class="btn btn-default btn-sm" data-container="body" title="Delete">
                    <i class="fa fa-trash-o"></i></a>
                    <a href="../view" class="btn btn-default btn-sm" title="Back">
                  <i class="fa fa-arrow-left"></i></a>
                </div>
                <!-- /.btn-group -->
                
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <?php echo $this->sth['message'];?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>            
            <div class="box-footer">              
              <a href="../../index/deleteContact/<?php echo $this->sth['id'];?>" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</a>
              <a href="../view" type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>