
		  	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->title;?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
            <!-- form start -->
            <form role="form" method="post" action="<?php echo URL ?>usersCrud/input">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" value="<?php echo $this->name;?>"placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" name="email" class="form-control" id="email" value="<?php echo $this->email;?>" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="username">username</label>
                  <input type="text" name="username" class="form-control" id="username" value="<?php echo $this->username;?>"placeholder="Enter username">
                </div>
                <div class="form-group">
                  <label for="password">password</label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                </div>            
                <div class="form-group">
                  <label>Access Level</label>
                  <select name="level" class="form-control">
                   <?php foreach ($this->sth as $b){ 
                      if ($this->level == $b['no']){
                        ?>
                  <option value="<?php echo $b['no'];?>" selected><?php echo $b['name'];?></option>
                        <?php
                      }
                      if ($this->level != $b['no']){
                        ?>
                  <option value="<?php echo $b['no'];?>"><?php echo $b['name'];?></option>
                        <?php 
                      }                  
                   } ?>   
                  </select>
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">
                <button type="submit" class="btn btn-success">Input</button>
                <a href="<?php echo URL ?>usersCrud/view" class="btn btn-primary">Cancel</a>
                <?php echo $this->msg; ?>
              </div>
            </form>
          </div>
		  

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->