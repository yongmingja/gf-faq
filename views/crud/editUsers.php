	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
            <!-- form start -->
            <form role="form" method="post" name="myForm" action="<?php echo URL ?>usersCrud/editProc">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" class="form-control" id="name" value="<?php echo $this->name;?>" required>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" name="email" class="form-control" id="email" value="<?php echo $this->email;?>" required>
                </div>
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" name="username" class="form-control" id="username" value="<?php echo $this->username;?>" required>
                </div>
                <div class="form-group" id="password1">
                  <label for="password" id="password2">Old Password </label>
                  <input type="password" name="oldPassword" class="form-control" id="password" placeholder="Enter current password" required>
                </div>
                <div class="form-group" id="password1">
                  <label for="password" id="password2">Password </label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Enter password" required>
                </div>
                <div class="form-group" id="repassword1">
                  <label for="password" id="repassword2">Confirm Password</label>
                  <input type="password" name="repassword" class="form-control" id="repassword" placeholder="Renter password" required>
                </div>          
                <div class="form-group">
                  <label>Access Level</label>
                  <select name="level" class="form-control">
                  <?php foreach ($this->sth as $b){ 
                      if ($this->level == $b['no']){
                        ?>
                  <option value="<?php echo $b['no'];?>" selected><?php echo $b['name'];?></option>
                        <?php
                      }
                      if ($this->level != $b['no']){
                        ?>
                  <option value="<?php echo $b['no'];?>"><?php echo $b['name'];?></option>
                        <?php 
                      }                  
                   } ?>   
                  </select>
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">
                <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $this->id;?>">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?php echo URL ?>usersCrud/view" class="btn btn-primary">Cancel</a>
                <?php echo $this->msg; ?>
              </div>
            </form>
          </div>
		  

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
