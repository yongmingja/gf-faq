
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Update Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
            <!-- form start -->
            <form role="form" method="post" action="<?php echo URL ?>categoryCrud/editProc">
              <div class="box-body">
                <div class="form-group">
                  <label for="cName">Category Name</label>
                  <input type="text" name="cName" class="form-control" id="cName" value="<?php echo $this->cName;?>">
                  <div class="form-group">
                  <label>Parent</label>
                    <?php $n = 0; foreach ($this->sth1 as $a){ 
                     if ($this->id == $a['parent']){$n++;}}
                     if($n > 0) {?>
                  <select name="parent" class="form-control" disabled>
                    <?php } else { ?>
                  <select name="parent" class="form-control">
                    <?php } ?>
                  <option value="0">root</option>
                    <?php foreach ($this->sth as $b){ 
                     if ($this->id == $b['id']){continue;}
                     elseif ($this->parent == $b['id']){?>
                  <option value="<?php echo $b['id'];?>" selected><?php echo $b['cName'];?></option>
                    <?php $no = 0;} 
                     if ($this->parent != $b['id'] and $this->id != $b['parent']) {
                    ?>                      
                  <option value="<?php echo $b['id'];?>"><?php echo $b['cName'];?></option>
                    <?php }} ?>   
                  </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">
                <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $this->id;?>">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?php echo URL ?>categoryCrud/view" class="btn btn-primary">Cancel</a>
                <?php echo $this->msg; ?>
              </div>
            </form>
          </div>
		  

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
