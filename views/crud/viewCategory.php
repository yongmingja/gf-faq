<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar Category</li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
    <a href="./" class="btn btn-primary">Add Content</a>
    <br />
    <br />
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Created</th>
                  <th>Modified</th>
                  <th>Parent</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php $no = 1;$child = 0;
foreach ($this->sth as $b){ 
  $parent=$b['parent'];
  if ($parent == 0) {
    foreach ($this->sth3 as $d) {
      if ($b['id'] == $d['parent']) {
        $child++;
      } 
    }
  }
?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $b['cName'];?></td>
                  <td><?php echo $b['created_by']." (".$b['Created'].")";?></td>
                  <td><?php echo $b['modified_by']." (".$b['Modified'].")";?></td>
                  <td><?php 
                  if ($parent != 0) {
                    foreach ($this->sth2 as $c) {
                      if ($parent == $c['id']) {
                        echo $c['cName'];
                      } 
                    }
                  } else {
                    echo "root";
                  }                       
                  ?></td>
                  <td>
                  <?php if($b['parent'] == 0 and $child > 0 ) {?>
                      <a href="edit/<?php echo $b['id'];?>"  title="Edit">
                          <span class="label label-success"><i class="fa fa-pencil-square-o"></i></span>
                      </a>
                      <btn disabled>
                          <span class="label" style="color:gray;"><i class="fa fa-trash-o"></i></span>
                      </btn>
                  <?php } 
                   else { ?>
                      <a href="edit/<?php echo $b['id'];?>"  title="Edit">
                          <span class="label label-success"><i class="fa fa-pencil-square-o"></i></span>
                      </a>
                      <a onclick="return confirm('Are You Sure ?')" href="delete/<?php echo $b['id'];?>" title="Delete">
                          <span class="label label-danger"><i class="fa fa-trash-o"></i></span>
                      </a>
                  <?php } ?>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
