<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Q &amp; A
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar Q &amp; A</li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
    <a href="./" class="btn btn-primary">Add Content</a>
    <br />
    <br />
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Question</th>
                  <th>Answer</th>
                  <th>Created</th>
                  <th>Modified</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php 
$no = 1;
foreach ($this->sth as $b){ 
?>
                <tr style="height: 30px;">
                  <td><?php echo $no++; ?></td>
                  <td><?php echo character_limiter($b['question'], 25); ?></td>
                  <td style="overflow: hidden;"><?php echo character_limiter($b['answer'], 60); ?></td>
                  <td><?php echo $b['created_by']." (".$b['Created'].")";?></td>
                  <td><?php echo $b['modified_by']." (".$b['Modified'].")";?></td>
                  <td>
                      <a href="edit/<?php echo $b['id'];?>" title="Edit">
                          <span class="label label-success"><i class="fa fa-pencil-square-o"></i></span>
                      </a>
                      <a onclick="return confirm('Are You Sure ?')" href="delete/<?php echo $b['id'];?>" title="Delete">
                          <span class="label label-danger"><i class="fa fa-trash-o"></i></span>
                      </a>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
