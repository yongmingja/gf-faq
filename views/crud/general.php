<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

tinymce.init({
      selector: "#post_content",
      theme: 'modern',
      relative_urls: false,
      remove_script_host: false,
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
      toolbar2: "print preview forecolor backcolor emoticons",
      image_advtab: true,
      plugins: [
         "advlist autolink lists link charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen",
         "insertdatetime nonbreaking save table contextmenu directionality",
         "emoticons template paste textcolor colorpicker textpattern"
      ],
   });

</script>
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL;?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->title;?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box box-primary">
            <!-- form start -->
            <form role="form" method="post" action="<?php echo URL; ?>settings/editProc/<?php echo $this->variable;?>" enctype = "multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="value"><?php echo $this->title;?></label>
                  
                  <?php if ($this->variable == "header") { ?>
                  <input type="text" name="value" class="form-control" disabled id="value" value="<?php echo $this->value;?>">
                  <span>Dimensi: 1350x310</span><br><br>
                  <input type="file" name="image">

                  <?php } else if ($this->variable == "favicon" or $this->variable == "site_logo") { ?>
                  <input type="text" name="value" class="form-control" disabled id="value" value="<?php echo $this->value;?>">
                  <br>
                  <input type="file" name="image">

                  <?php } else if ($this->group == "link") { ?>
                  <input type="text" name="value" class="form-control" id="value" value="<?php echo $this->value;?>">
                  <label for="description">Name</label>
                  <input type="text" name="description" class="form-control" id="description" value="<?php echo $this->description;?>">
                  <br>
                  
                  <?php } else if ($this->variable == "site_maintenance") { ?>                
                  <select name="value" class="form-control">
                    <option value="open">Open</option>
                    <option value="close">Close</option>
                  </select>

                  <?php } else if ($this->variable == "main_address") { ?>
                  <textarea name="value" class="form-control" id="post_content" rows="6"><?php echo $this->value;?></textarea>
                  <?php } else { ?>
                  <input type="text" name="value" class="form-control" id="value" value="<?php echo $this->value;?>">
                  <?php } ?>
                  <input type="hidden" name="variable" class="form-control" id="variable" value="<?php echo $this->variable;?>">
                </div>
              </div>
              <!-- /.box-body -->
              
              <div class="box-footer">                
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?php 
                  if ($this->group == "general") { echo URL.'settings'; }
                  elseif ($this->group == "social_account") { echo URL.'settings/social'; }
                  elseif ($this->group == "link") { echo URL.'settings/link'; } 
                ?>" class="btn btn-primary">Cancel</a>
              </div>
            </form>
          </div>
		  <?php echo $this->msg; ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
