<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->title;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->title;?></li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Judul</th>
                  <th>Date</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php 
$no = 1;
foreach ($this->sth as $b){ 
?>
                <tr style="height: 30px;">
                  <td><?php echo $no++; ?></td>
                  <td><?php echo character_limiter($b['first_name'].' '.$b['last_name'], 25); ?></td>
                  <td><?php echo character_limiter($b['email'], 25); ?></td>
                  <td style="width: 320px; overflow: hidden;"><?php echo character_limiter($b['judul'], 60); ?></td>
                  <td><?php echo $b['date_sent']; ?></td>
                  <td style="width: 10%;">
                      <a href="viewOneContact/<?php echo $b['id'];?>">
                    <?php
                          if ($b['view'] == 0) {?> 
                            <span class="label label-primary"><i class="fa fa-envelope"></i></span> 
                    <?php } elseif ($b['view'] == 1) { ?>
                            <span class="label label-info"><i class="fa fa-envelope-open"></i></span>
                    <?php } ?>                          
                      </a>
                      <a onclick="return confirm('Are You Sure ?')" href="deleteContact/<?php echo $b['id'];?>">
                          <span class="label label-danger"><i class="fa fa-trash-o"></i></span>
                      </a>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
