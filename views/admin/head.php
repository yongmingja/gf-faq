<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->title;?></title>
  <link rel="shortcut icon" href="<?php echo URL; ?>public/images/gfsoft.svg">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL;?>views/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL;?>views/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL;?>views/admin/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo URL;?>views/admin/dist/css/skins/_all-skins.min.css">
  <!-- jQuery 3 -->
  <link href="<?php echo URL;?>views/admin/bower_components/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo URL;?>views/admin/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo URL;?>views/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>

  <?php 
    $url = isset($_GET['url']) ? $_GET['url'] : null;
    $url = rtrim($url, '/');
    $url = explode('/', $url);
    if (isset($url[1])) {
      if ($url[1] == "view") {
        ?>
<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo URL;?>views/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <?php
      }
    }
//Character Limiter
if ( ! function_exists('character_limiter'))
{
  function character_limiter($str, $n = 500, $end_char = '&#8230;')
  {
    if (strlen($str) < $n)
    {
      return $str;
    }

    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

    if (strlen($str) <= $n)
    {
      return $str;
    }

    $out = "";
    foreach (explode(' ', trim($str)) as $val)
    {
      $out .= $val.' ';

      if (strlen($out) >= $n)
      {
        $out = trim($out);
        return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
      }
    }
  }
}
    function viewMsg() {
    $con = new Database();
    $query = "SELECT * FROM `contact` ORDER BY `date_sent` DESC";
    $sth = mysqli_query($con, $query);
    return $sth;
    }

    $msg = viewMsg();
    
    $no = 0;
    foreach ($msg as $row) {
      if ($row['view'] == 0) {
        $no++;
      }
    }
  ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo URL;?>" class="logo" target="_blank">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-globe"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>View</b>Site</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <?php if ($no !== 0) { ?>
              <span class="label label-success"><?php echo $no;?></span>
              <?php }?>
            </a>
            
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $no;?> new messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php $limit = 0; foreach($msg as $row) { $limit++; if ($limit < 6) {?>
                  <li><!-- start message -->
                    <a href="<?php echo URL.'index/viewOneContact/'.$row['id'];?>">
                      <div class="pull-left">
                        <?php if ($row['view'] == 0) { ?>
                        <div class="btn btn-sm"><i class="fa fa-envelope" style="color: #367fa9;"></i></div>                        
                        <?php } elseif ($row['view'] == 1) { ?>
                        <div class="btn btn-sm"><i class="fa fa-envelope-open" style="color: grey;"></i></div>                        
                        <?php } ?>
                      </div>
                      <h4>
                        <?php echo $row['first_name']." ".$row['last_name'];?>
                        <small><i class="fa fa-clock-o"></i> <?php

$date1=date_create($row['date_sent']);
$date2=date_create(date("Y-m-d H:i:s"));
$diff=date_diff($date1,$date2); 
if ($diff->format("%Y%M%D%H%I%S") <= '000000000059')
  { echo $diff->format("%s detik"); }
elseif ($diff->format("%Y%M%D%H%I%S") <= '000000005959')
  { echo $diff->format("%i menit"); }
elseif ($diff->format("%Y%M%D%H%I%S") <= '000000235959')
  { echo $diff->format("%h jam"); }
elseif ($diff->format("%Y%M%D%H%I%S") <= '000012235959')
  { echo $diff->format("%d hari"); }
elseif ($diff->format("%Y%M%D%H%I%S") <= '003012235959')
  { echo $diff->format("%m bulan"); }
elseif ($diff->format("%Y%M%D%H%I%S") > '003012235959')
  { echo $diff->format("%y tahun"); }
 ?></small>
                      </h4>
                      <p><?php echo character_limiter($row['judul'], 20);?></p>
                    </a>
                  </li>
                  <?php }}?>
                  <!-- end message -->                  
                </ul>
              </li>
              <li class="footer"><a href="<?php echo URL;?>index/view">See All Messages</a></li>
            </ul>
          </li>
          
          <?php 
            Session::init();
                $session = Session::get('userID');
                if(isset($session))
                {
                    // User exists
                    $db = new Database();
                    $sql = "SELECT * FROM users WHERE id=$session LIMIT 1";

                    // RUN THE MYSQL QUERY TO FETCH THE USER, SAVE INTO $row
                $sth = $db->query($sql);
                $row = mysqli_fetch_assoc($sth);
                }
          ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo $row['name'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <p>
                  <?php if ($row['level'] == 0) {
                    echo "Admin";
                  }elseif ($row['level'] == 1) {
                    echo "Support";
                  }?>                
                </p>
              </li>             
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo URL.'usersCrud/edit/'.$row['password'];?>" class="btn btn-default btn-flat">
                  Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo URL;?>dashboard/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo URL;?>dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-object-group"></i> <span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL; ?>categoryCrud/view"><i class="fa fa-circle-o text-red"></i> View All</a></li>
            <li><a href="<?php echo URL; ?>categoryCrud"><i class="fa fa-circle-o"></i> Insert</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-comments"></i> <span>Q&amp;A</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL; ?>qaCrud/view"><i class="fa fa-circle-o text-red"></i> View All</a></li>
            <li><a href="<?php echo URL; ?>qaCrud"><i class="fa fa-circle-o"></i> Insert</a></li>
          </ul>
        </li>
        <?php if ($this->role == 0):?>
        <li class="header">Setting</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL; ?>usersCrud/view"><i class="fa fa-circle-o text-red"></i> View All</a></li>
            <li><a href="<?php echo URL; ?>usersCrud"><i class="fa fa-circle-o"></i> Insert</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-sliders"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo URL; ?>settings"><i class="fa fa-circle-o text-white"></i> General</a></li>
            <li><a href="<?php echo URL; ?>settings/link"><i class="fa fa-link"></i> Link</a></li>
            <li><a href="<?php echo URL; ?>settings/social"><i class="fa fa-users"></i> Socmed</a></li>
            <li><a href="<?php echo URL; ?>dashboard/showBackUpDB"><i class="fa fa-archive"></i> Backup &amp; Restore</a></li>
          </ul>
        </li>
        <?php endif; ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
