<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Back Up &amp; Restore
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Back Up &amp; Restore</li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
    <a href="runBackUp" class="btn btn-primary">New Backup</a>
    <br />
    <br />
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php $no = 1; $i = 0;

foreach ($this->sth as $b){ 

    if ($no == count($this->sth)-1){
        continue;
    }
?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $b[5].$b[6].$b[7].$b[8].$b[9].$b[10].$b[11].$b[12];?></td>
                  <td>
                      <a onclick="return confirm('File backup akan terhapus permanen,\nlanjut restore?')" href="runRestore/<?php echo $this->sth[$i++];?>">
                          <span class="label label-success"><i class="fa fa-upload"></i></span>
                      </a>
                      <a onclick="return confirm('File backup akan terhapus permanen,\nlanjut hapus?')" href="deleteDb/<?php echo $this->sth[$i++];?>">
                          <span class="label label-danger"><i class="fa fa-trash-o"></i></span>
                      </a>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
