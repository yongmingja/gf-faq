<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category Table
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th width="50%">Nilai</th>
                  <?php if (mysqli_fetch_assoc($this->sth)['group'] == "link") {?>
                  <th width="25%">Nama Link</th>
                  <?php } ?>
                  <th class="text-center" width="10%">Aksi</th>
                </tr>
                </thead>
                <tbody>
<?php $no = 1;
foreach ($this->sth as $b){ 
?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $b['variable'];?></td>
                  <td><?php echo $b['value'];?></td>
                  <?php if ($b['group'] == "link") {?>
                  <th width="25%"><?php echo $b['description'];?></th>
                  <?php } ?>
                  <td class="text-center">
                      <a href="<?php echo URL;?>settings/edit/<?php echo $b['variable'];?>">
                          <span class="label label-success"><i class="fa fa-pencil-square-o"></i></span>
                      </a>
                      <a onclick="return confirm('Restore to default ?')" href="<?php echo URL;?>settings/reset/<?php echo $b['variable'];?>">
                          <span class="label label-danger"><i class="fa fa-refresh"></i></span>
                      </a>
                  </td>
                </tr>
<?php } ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
