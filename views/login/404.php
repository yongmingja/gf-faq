

  <script type="text/javascript">
  function setValue() {
    document.getElementById("search").value="";
  }
</script>
<br>
  <nav class="container nav-wrapper" style="box-shadow: none; background-color:#fff;">
  <div class="row">
  <div class="col s7">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="<?php echo URL ?>" class="breadcrumb">GF-Help</a>
        <span class="breadcrumb" style="color: #627b8fb3  ;"><?php echo $this->msg; ?></span>
        </div>
    </div>
  </div>
  <div class="col s5">
    <nav class="searchBar">
        <div class="nav-wrapper">
          <form method="post" action='<?php echo URL ?>search'>
            <div class="input-field">
              <input id="search" name="search" type="search" placeholder="Search" required>
              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
              <i class="material-icons"  onclick="setValue()">close</i>
            </div>
          </form>
        </div>
    </nav>
  </div>
  </div>
  </nav>
<br><br><br><br>

  <div class="container backcover2">
  <div class="section" style="padding-top: 0;">
    <div class="row">
      <div class="col s12 setting1"><span class="flow-text">404 Error Page</span></div>
    </div>
  </div>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="<?php echo URL;?>">return to homepage</a> or try using the search form.
          </p>
      
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  </div>