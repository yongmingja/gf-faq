<script type="text/javascript">
	function setValue() {
		document.getElementById("search").value="";
	}
</script>
<br>
  <nav class="container nav-wrapper" style="box-shadow: none; background-color:#fff;">
  <div class="row">
  <div class="col s7">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="<?php echo URL ?>" class="breadcrumb">GF-Help</a>
        <span class="breadcrumb" style="color: #627b8fb3  ;"><?php echo $this->row['cName']; ?></span>
        </div>
    </div>
  </div>
  <div class="col s5">
    <nav class="searchBar">
        <div class="nav-wrapper">
          <form method="post" action='<?php echo URL ?>search'>
            <div class="input-field">
              <input id="search" name="search" type="search" placeholder="search" required>
              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
              <i class="material-icons"  onclick="setValue()">close</i>
            </div>
          </form>
        </div>
    </nav>
  </div>
  </div>
  </nav>
<br><br><br><br>
  <div class="container backcover2">
  <div class="section" style="padding-top: 0;">
    <div class="row">
      <div class="col s12 setting1"><span class="flow-text"><?php echo $this->row['cName']; ?></span></div>
    </div>
  </div>
        <div class="">
             <ul class="collapsible" data-collapsible="accordion" style="box-shadow: none; border: none;">
              <?php foreach ($this->sth as $a) { ?>
              <li>
                <div class="collapsible-header"><i id="hd1" class="material-icons">chevron_right</i><?php echo $a['cName'];?></div>
                <div class="collapsible-body">
                  <ul>
                    <?php                     
                    $name = $a['cName'];
                    $query = "SELECT * FROM `qa` WHERE category='$name'";
                    $sub = $this->db->query($query);
                    $row = mysqli_fetch_assoc($sub);
                    $count = 0;
                    if (mysqli_num_rows($sub) > 0 ) {
                      foreach ($sub as $s) {                        
                        $count++;
                        if($count <= 10){
                     ?>
                    <li><a href="search?id=<?php echo $s['id'];?>"><span style="font-size: 140%;">&rsaquo; </span><?php echo $s['question'];?></a></li>
                    <?php }}} else {
                      echo "No result";}?>
                  </ul>
                </div>
              </li>
              <?php } ?>
            </ul>
          </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  </div>