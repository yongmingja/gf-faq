
  <footer class="page-footer" style="background-color: #2A3F54CC">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">PT. GFSoft Indonesia</h5>

          <?php echo mysqli_fetch_assoc(viewPart("main_address"))['value'];?>

        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Situs kami</h5>
          <ul>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("link_1"))['value']) > 15) {
              echo '<li><a class="white-text hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("link_1"))['value'].'"><span style="font-size: 150%;">&raquo;</span> '.mysqli_fetch_assoc(viewPart("link_1"))['description'].'</a></li>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("link_2"))['value']) > 15) {
              echo '<li><a class="white-text hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("link_2"))['value'].'"><span style="font-size: 150%;">&raquo;</span> '.mysqli_fetch_assoc(viewPart("link_2"))['description'].'</a></li>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("link_3"))['value']) > 15) {
              echo '<li><a class="white-text hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("link_3"))['value'].'"><span style="font-size: 150%;">&raquo;</span> '.mysqli_fetch_assoc(viewPart("link_3"))['description'].'</a></li>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("link_4"))['value']) > 15) {
              echo '<li><a class="white-text hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("link_4"))['value'].'"><span style="font-size: 150%;">&raquo;</span> '.mysqli_fetch_assoc(viewPart("link_4"))['description'].'</a></li>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("link_5"))['value']) > 15) {
              echo '<li><a class="white-text hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("link_5"))['value'].'"><span style="font-size: 150%;">&raquo;</span> '.mysqli_fetch_assoc(viewPart("link_5"))['description'].'</a></li>';
            } ?>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text floating">Ikuti kami</h5>
          <ul>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("facebook"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("facebook"))['value'].'"><i class="fa fa-facebook"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("google_plus"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("google_plus"))['value'].'"><i class="fa fa-google-plus"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("instagram"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("instagram"))['value'].'"><i class="fa fa-instagram"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("linked_in"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("linked_in"))['value'].'"><i class="fa fa-linkedin"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("twitter"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("twitter"))['value'].'"><i class="fa fa-twitter"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("youtube"))['value']) > 15) {
              echo '<a class="btn-floating btn-small waves-effect waves-light blue-grey darken-1 hoverable" target="_blank" href="'.mysqli_fetch_assoc(viewPart("youtube"))['value'].'"><i class="fa fa-youtube"></i></a>';
            } ?>        
          </ul>
          
        </div>
      </div>
    </div>
    <div class="footer-copyright" style="background-color: #2A3F54E6;">
      <div class="container">
        <?php echo mysqli_fetch_assoc(viewPart("site_footer"))['value'];?>
      </div>
    </div>
  </footer>

    
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo URL; ?>public/js/materialize.js"></script>

    <script type="text/javascript" src="<?php echo URL; ?>vendors/materialize/bin/materialize.js"></script>
    <script src="http://materializecss.com/templates/starter-template/js/init.js"></script>
  </body>
</html>