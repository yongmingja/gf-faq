<?php
    
    function viewPart($id) {
    $con = new Database();
    $query = "SELECT * FROM `settings` WHERE `variable`='$id'";
    $sth = mysqli_query($con, $query);
    return $sth;
    }  
    if (mysqli_fetch_assoc(viewPart("site_maintenance"))['value'] == "close") {
      header('location: '.URL.'index/undermaintenance');  
    }    

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="<?php echo mysqli_fetch_assoc(viewPart("meta_keywords"))['value'];?>"/>
    <meta name="description" content="<?php echo mysqli_fetch_assoc(viewPart("meta_description"))['value'];?>"/>
    <meta name="subject" content="Situs Perusahaan">
    <meta name="copyright" content="<?php echo mysqli_fetch_assoc(viewPart("company_name"))['value'];?>">
    <meta name="language" content="Indonesia">
    <meta name="robots" content="index,follow" />
    <meta name="Classification" content="Business">
    <meta name="url" content="<?php echo mysqli_fetch_assoc(viewPart("site_url"))['value'];?>">
    <meta name="identifier-URL" content="<?php echo mysqli_fetch_assoc(viewPart("site_url"))['value'];?>">
    <meta name="category" content="Help Center, Business">
    <meta name="coverage" content="Worldwide">
    <meta name="distribution" content="Global">
    <meta name="rating" content="General">
    <!--Import Google Icon Font-->
    <script src="<?php echo URL; ?>vendors/jquery/js/jquery-3.3.1.slim.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo URL; ?>public/images/<?php echo mysqli_fetch_assoc(viewPart("favicon"))['value'];?>">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo URL; ?>vendors/materialize/css/materialize.min.css"  media="screen,projection"/>
<!--     <link type="text/css" rel="stylesheet" href="<?php echo URL; ?>vendors/materialize/css/ghpages-materialize.css"  media="screen,projection"/>
 -->    <link href="<?php echo URL; ?>vendors/materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL;?>views/admin/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/custom.css">
    <title>GF-FAQ</title>
  </head>
  <body>  
    <nav class="nav" role="navigation">
      <div class="nav-wrapper container"><a id="logo-container" href="<?php echo URL; ?>" class="brand-logo">
        <img src="<?php echo URL;?>public/images/<?php echo mysqli_fetch_assoc(viewPart("site_logo"))['value'];?>" height="50px" width="auto" style="padding: 10px;"> 
        </a>      
        <ul class="right hide-on-med-and-down">
          <li><a href="<?php echo URL; ?>index/contact" style="font-weight: 600;">Contact Us</a></li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
          <li><a href="<?php echo URL; ?>index/contact" style="font-weight: 600;">Contact Us</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      </div>
    </nav>
