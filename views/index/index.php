<script type="text/javascript">
	function setValue() {
		document.getElementById("search").value="";
	}
</script>
<style type="text/css">
  .backcover {
    background: url(public/images/<?php echo mysqli_fetch_assoc(viewPart("header"))['value'];?>);
  }
</style>
<div class="backcover">
  <div class="section no-pad-bot">
    <div class="container">
      <h2 class="header center" style="color:#ffffffe6;">GF-Help Center</h2>
      <div class="row center">
      <div class="col s2"></div>
		<div class="col s8">
        <nav class="searchBar">
		    <div class="nav-wrapper">		  
		      <form method="post" action='<?php echo URL ?>search'>
		        <div class="input-field">
		          <input id="search" type="search" name="search" required>
		          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
		          <i class="material-icons"  onclick="setValue()">close</i>
		        </div>
		      </form>		      	
		    </div>
	  	</nav>
	  	</div>
		<div class="col s2"></div>	  	
      </div>
      <br><br><br>
    </div>
  </div>
</div>
<br>
  <div class="container backcover2">
  <style>
  @media(min-width: 600px) {#cols {
    width: 49% !important;
    display: inline-block !important;
    float: none !important;
  }}
  @media(min-width: 992px) {#cols {
    width: 32.333% !important;
    display: inline-block !important;
    float: none !important;
  }}
  
  </style>
    <div class="section" style="padding-top: 0; background-color:#fff">
    <div class="row">
      <div class="col s12 setting1"><span class="flow-text">Featured articles</span></div>
    </div>
      <!--   Icon Section   -->
      <div class="row">
      <center>
      <?php $no = 0; foreach ($this->sth as $b){ //print root tabs
        
        $no = $no +1;
        if ($no) {
        $id = $b['id'];
        $count = 0;
          foreach ($this->sth2 as $c){ //check tabs is parent
            if ($b['id'] == $c['parent']){
              $count = $count +1;
            }           
          }
          if ($count > 0 and $b['parent'] == 0) {          
      ?>
        <div class="col s12 m6 l4 xl3" id="cols">
          <div class="icon-block">
            <ul id="<?php echo $b['cName'];?>" class="dropdown-content">
              <?php 
                foreach ($this->sth3 as $d) {
                  if ($id == $d['parent']){
                    ?>
              <li><a href="category/sub/<?php echo $d['cName'];?>"><?php echo $d['cName'];?></a></li>
                    <?php
                  }
                }
              ?>            
            </ul>            
            <h2 class="center light-blue-text "><a class="btn dropdown-button btn-large waves-effect waves-light" href="category?id=<?php echo $b['id'];?>" data-beloworigin="true" data-hover="true" data-activates="<?php echo $b['cName'];?>" id="<?php echo $b['cName'];?>"><?php echo $b['cName'];?><i class="material-icons right">arrow_drop_down</i></a></h2>
          </div>
        </div>
      <?php } else if ($count == 0) {
      if ($b['parent'] == 0) { ?>
      <div class="col s12 m6 l4 xl3" id="cols">
        <div class="icon-block">
          <h2 class="center light-blue-text "><a class="btn dropdown-button btn-large waves-effect waves-light" href="javascript:void(0)"><?php echo $b['cName'];?></a></h2>
        </div>
      </div>
    <?php }}}} ?>
      </div>
    </div>
    <br><br>

  </div>
