<script type="text/javascript">
  function setValue() {
    document.getElementById("search").value="";
  }
</script>
<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo URL;?>image/assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

tinymce.init({
      selector: "#textarea1",
      theme: 'modern',
      paste_data_images:true,
      relative_urls: false,
      remove_script_host: false,
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
      toolbar2: "print preview forecolor backcolor emoticons",
      image_advtab: true,
      plugins: [
         "advlist autolink lists link charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars code fullscreen",
         "insertdatetime nonbreaking save table contextmenu directionality",
         "emoticons template paste textcolor colorpicker textpattern"
      ],
      automatic_uploads: true,
      //images_upload_path: 'http://localhost/faq/uploads/images/qa',
      images_upload_url: 'http://localhost/faq/image/blog/posts/tinymce_upload_handler',
      file_picker_types: 'image', 
      
      file_picker_callback: function(cb, value, meta) {
         var input = document.createElement('input');
         input.setAttribute('type', 'file');
         input.setAttribute('accept', 'image/*');
         input.onchange = function() {
            var file = this.files[0];
            console.log(file);
            var reader = new FileReader();            
            reader.readAsDataURL(file);
            reader.onload = function () {
               var id = 'post-image-' + (new Date()).getTime();
               var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
               var blobInfo = blobCache.create(id, file, reader.result);
               blobCache.add(blobInfo);
               cb(blobInfo.blobUri(), { title: file.name });
               console.log(reader.result);
            };
          console.log(reader);
          console.log(reader.result);
         };
         input.click();
         

      }
   });

</script>
<br>
  <nav class="container nav-wrapper" style="box-shadow: none; background-color:#fff;">
  <div class="row">
  <div class="col s7">
    <div class="nav-wrapper">
      <div class="col s12">
        <a href="<?php echo URL ?>" class="breadcrumb">GF-Help</a>
        <span class="breadcrumb" style="color: #627b8fb3  ;"><?php echo $this->title; ?></span>
        </div>
    </div>
  </div>
  <div class="col s5">
    <nav class="searchBar">
        <div class="nav-wrapper">
          <form method="post" action='<?php echo URL ?>search'>
            <div class="input-field">
              <input id="search" name="search" type="search" placeholder="Search" required>
              <label class="label-icon" for="search"><i class="material-icons">search</i></label>
              <i class="material-icons"  onclick="setValue()">close</i>
            </div>
          </form>
        </div>
    </nav>
  </div>
  </div>
  </nav>
<div class="container">
<br>
<div class="row">
    <div class="col s12 m12 l6 xl6">
        <div>
            <div class="" style="font-size: 200%;">
                    Hubungi kami
            </div>
            <div class="icon-contacts pb-3">                        
                <p class="mbr-text align-left mbr-fonts-style display-7">
                    Phone: <?php echo mysqli_fetch_assoc(viewPart("site_phone"))['value'] ?><br>
                    Email: <a href="mailto:<?php echo mysqli_fetch_assoc(viewPart("site_mail"))['value'] ?>"><?php echo mysqli_fetch_assoc(viewPart("site_mail"))['value'] ?></a>
                </p>
            </div>
        </div>
        <div class="google-map"><iframe frameborder="0" width="100%" height="400px" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15956.14784155093!2d104.0478701!3d1.1339495500000112!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x61c0efb3951609c0!2sPT.+GFSoft+Indonesia!5e0!3m2!1sen!2sid!4v1521787966004&zoom=13" allowfullscreen=""></iframe></div>                
    </div>         
    <div class="col s12 m12 l6 xl6">
        <div data-form-type="formoid">
            <form class="block mbr-form" action="<?php echo URL;?>index/contactProc" method="post">
            <div class="row">
              <div class="input-field col s6">
			          <input id="first_name" name="first_name" type="text" class="validate" required="" value="<?php echo $this->fname;?>">
			          <label for="first_name">First Name</label>
			        </div>
			        <div class="input-field col s6">
			          <input id="last_name" name="last_name" type="text" class="validate" required="" value="<?php echo $this->lname;?>">
			          <label for="last_name">Last Name</label>
			        </div>
			        <div class="input-field col s12">
			          <input id="email" name="email" type="text" class="validate" required="" value="<?php echo $this->email;?>">
			          <label for="email">Email</label>
			        </div>
              <div class="input-field col s12">
                <input id="judul" name="judul" type="text" class="validate" required="" value="<?php echo $this->judul;?>">
                <label for="judul">Judul</label>
              </div>
			        <div class="input-field col s12">
                      <label>Pesan</label>
			          <textarea id="textarea1" name="pesan" class="materialize-textarea validate" style="height: 200px;"><?php echo $this->pesan;?></textarea>
			          
			        </div>
			        <div class="input-field col s6">
			          <?php echo '<img src="' .$this->captcha['image_src'] . '" alt="CAPTCHA code" style="height: 48px; width: 100%;">';?>
			        </div>
			        <div class="input-field col s6">
			          <input id="captcha" type="text" class="validate" name="captcha" required="">
			          <label for="captcha">Captah</label>
			        </div>

              <div class="input-field col s6">
                <input type="hidden" name="captchaValidate" value="<?php echo $this->captcha['code'];?>">
                <input type="hidden" name="ip_address" value="<?php echo $this->clientIP;?>">
                <button class="btn" type="submit" name="action" id="validate">Kirim
                  <i class="material-icons right">send</i>
                </button>
              </div><?php echo $this->msg;?>
            </div>
            </form>                    
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
  if (document.getElementById('btnWarning').textContent === 'Success') {
    document.getElementById('first_name').value = '';
    document.getElementById('last_name').value = '';
    document.getElementById('judul').value = '';
    document.getElementById('email').value = '';
    document.getElementById('textarea1').textContent = '';
  }
</script>