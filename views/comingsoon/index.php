<?php
    
    function viewPart($id) {
    $con = new Database();
    $query = "SELECT * FROM `settings` WHERE `variable`='$id'";
    $sth = mysqli_query($con, $query);
    return $sth;

    }    
    if (mysqli_fetch_assoc(viewPart("site_maintenance"))['value'] == "open") {
      header('location: '.URL);  
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>GF-FAQ</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
    <link rel="shortcut icon" href="../public/images/<?php echo mysqli_fetch_assoc(viewPart("favicon"))['value'];?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>views/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>views/admin/bower_components/font-awesome/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>views/comingsoon/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL;?>views/comingsoon/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
		<div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('../public/images/<?php echo mysqli_fetch_assoc(viewPart("header"))['value'];?>');">
				<iframe frameborder="0" width="1000px" height="85%" style="border: 10px solid #6e6e6e;border-radius: 30px;" src="//www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15956.14784155093!2d104.0478701!3d1.1339495500000112!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x61c0efb3951609c0!2sPT.+GFSoft+Indonesia!5e0!3m2!1sen!2sid!4v1521787966004&zoom=13" allowfullscreen=""></iframe>
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
			<div class="wrap-pic1">
				<img src="../public/images/<?php echo mysqli_fetch_assoc(viewPart("favicon"))['value'];?>" alt="LOGO">
			</div>

			<div class="p-t-50 p-b-60">
				<p class="m1-txt1 p-b-36">
					Our website is <span class="m1-txt2">Maintenance</span>, Contact Us.
				</p>
				<p class="mbr-text align-left mbr-fonts-style display-7">
                    Phone: <?php echo mysqli_fetch_assoc(viewPart("site_phone"))['value'] ?><br>
                    Email: <a href="mailto:<?php echo mysqli_fetch_assoc(viewPart("site_mail"))['value'] ?>"><?php echo mysqli_fetch_assoc(viewPart("site_mail"))['value'] ?></a>
                </p>
				<?php echo mysqli_fetch_assoc(viewPart("main_address"))['value'];?>

				<p class="s2-txt3 p-t-18">
					
				</p>
			</div>

			<div class="flex-w">
			<?php if(strlen(mysqli_fetch_assoc(viewPart("facebook"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg3 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("facebook"))['value'].'"><i class="fa fa-facebook"></i></a>';
            } ?> 
            <?php if(strlen(mysqli_fetch_assoc(viewPart("google_plus"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg7 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("google_plus"))['value'].'"><i class="fa fa-google-plus"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("instagram"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg8 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("instagram"))['value'].'"><i class="fa fa-instagram"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("linked_in"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg6 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("linked_in"))['value'].'"><i class="fa fa-linkedin"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("twitter"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg4 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("twitter"))['value'].'"><i class="fa fa-twitter"></i></a>';
            } ?>
            <?php if(strlen(mysqli_fetch_assoc(viewPart("youtube"))['value']) > 15) {
              echo '<a class="flex-c-m size5 bg5 how1 trans-04 m-r-5" target="_blank" href="'.mysqli_fetch_assoc(viewPart("youtube"))['value'].'"><i class="fa fa-youtube"></i></a>';
            } ?>   
			</div>
		</div>
	</div>



	

</body>
</html>